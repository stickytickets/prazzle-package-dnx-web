var fs = require('fs');
var installer = require('./install.js');
var async = require('async');

var baseDir = process.argv[2];

var log = require('winston');

log.level = 'debug';

if(!baseDir){
	console.error('Please set the base directory');
	return;
}

if (!fs.existsSync(baseDir)) {
    console.error('Directory not found');
    return;
}

var webDir = `${baseDir}/web`;

var setupPublicSite = function(cb){

	var publicSiteDir = `${webDir}/src/PublicSite`;
	var publicSiteOptions = {
		environment: 'dev',
		project: 'web',
		host: 'public.localhost.com',
		dir: publicSiteDir,
		enable32Bit: true,
		log: log
	}

	installer(publicSiteOptions, cb);
	//cb();
}

var setupApiSite = function(_, cb) {
	var apiSiteDir = `${webDir}/src/Api`;

	var apiSiteOption = {
		environment: 'dev',
		project: 'api',
		port:18000,
		dir: apiSiteDir,
		enable32Bit: true,
		log: log
	}

	installer(apiSiteOption, cb);
}

var setupOrganiserSite = function(_, cb) {
	var organiserSiteDir = `${webDir}/src/Web`;

	var organiserSiteOptions = {
		environment: 'dev',
		project: 'organiser',
		port:8080,
		dir: organiserSiteDir,
		enable32Bit: true,
		log: log
	}

	installer(organiserSiteOptions, cb);
}

var setupPublicApiVDir = function(_, cb) {

	var options = {
		environment: 'dev',
		project: 'web',
		host: 'public.localhost.com',
		dir: `${baseDir}/api-hub/iisnode`,
		vdir: '/api',
		log: log
	};

	installer(options, cb);
}

var setupPublicOrganiserDir = function(_, cb) {

	var options = {
		environment: 'dev',
		project: 'web',
		host: 'public.localhost.com',
		dir: `${baseDir}/api-hub/iisnode`,
		vdir: '/organiser',
		log: log
	};

	installer(options, cb);
}



async.waterfall([
	setupPublicSite,
	setupApiSite,
	setupOrganiserSite,
	setupPublicApiVDir,
	setupPublicOrganiserDir
]);
