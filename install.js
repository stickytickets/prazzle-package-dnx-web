var spawn = require('child_process').spawn;
var path = require('path');
var log = require('winston');

module.exports = function(options, cb) {
    log.info('Running Install for dnx-web app: %s', options.project);
    return runInstall(options, cb);
}

function runInstall(options, cb) {


    /*
    	iis_install
    	* Website Name
    	* Port
    	* Host Headers
    	* vdir
    	* enable32bit
    	* b - additional bindings
    	* dir  - current direc

    */
    var siteName = (options.environment ? (options.environment + '-') : '') + options.project;

    var cmdOptions = {
        name: siteName,
        port: options.port || (options.packageJson && options.packageJson.deploy && options.packageJson.deploy['port']) || 80,
        host: options.host || options['website-host'] || '*',
        vdir: options.vdir || (options.packageJson && options.packageJson.deploy && options.packageJson.deploy['vdir']) || null,
        dir: options.dir || path.resolve(path.join(process.cwd(), 'wwwroot'))
    };

    var args = [];
    for (var k in cmdOptions) {
        if (cmdOptions[k]) args.push('--' + k + '=' + cmdOptions[k]);
    }

    if (options.enable32Bit) {
        args.push('--enable32bit');
    }

    log.verbose('Running IISWebsiteInstall with args: %s', args.join(' '))
    var cmd = spawn(path.resolve(path.join(__dirname, 'iiswebsiteinstaller/IISWebsiteInstall.exe')), args);
    var hasError;

    cmd.stdout.on('data', (data) => {
        log.verbose(data.toString());
    });

    cmd.stderr.on('data', (data) => {
        hasError = true;
        log.error(data.toString());
    });

    cmd.on('close', (code) => {
        if (hasError) return cb('An error occured during the "npm run install" command.');
        return cb(null, options);
    });
}
