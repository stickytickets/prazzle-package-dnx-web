var spawn = require('child_process').spawn;
var async = require('async');
var tmp = require('tmp');
var fs = require('fs');
var path = require('path');
var rimraf = require('rimraf');
var log = require('winston');

module.exports = function(options, cb)
{
	log.info('Packing Dnx Console App To: %s', options.outputDir);

	//return if already exists.
	if(fs.existsSync(path.resolve(options.packageFilepath)))
		return cb(null, createResult(options));


	async.waterfall([
		async.apply(runDnuPublish, options),
		runDnuPublish,
		updateProjectJsonInPublishDir,
		getRequiredEnvironmentKeys,
		zipDirectory
	],function(err){

		if(err) return cb(err);

		cb(null, createResult(options));
	});

}


function createResult(options){
	return  [
		{
			file: path.resolve(options.packageFilepath),
			metadata:
			{
				name: options.project,
				version: options.version,
				extension: path.extname(options.packageFilepath),
				packageType: 'dnx-web',
				deployment: 'server',
				install: options.install,
				env: options.environmentKeys
			}
		}
	];
}


function cleanDir(options,cb){
	options.fn.cleanDir(options.outputDir, function(err){ cb(err,options); })
}

function runDnuPublish(options, cb){

	var dnu_publish = spawn('dnu.cmd', ['publish', '-o', options.tmpDir, '--no-source', '--configuration', 'Release' ]);
	var hasError;

	dnu_publish.stdout.on('data', (data) => {
		log.debug(data.toString());
	});

	dnu_publish.stderr.on('data', (data) => {
		hasError = true;
		log.error(data.toString());
	});

	dnu_publish.on('close', (code) => {

		if(hasError) return callback('An error occured during the dnu publish command.');

		log.info('DNU publish completed');

		return cb(null, options);
	});
}

function updateProjectJsonInPublishDir(options,cb)
{
	options.packageJson.name = options.packageJson.name || options.project;
	fs.writeFileSync(path.join(options.tmpDir,'project.json'), JSON.stringify(options.packageJson));
	cb(null, options);
}

function getRequiredEnvironmentKeys(options, cb)
{
	log.verbose('Reading environment keys...');

	options.environmentKeys = options.fn.envHelper.read('.env.required');

	cb(null, options);
}

function writeEnvironmentKeys(options, cb){

	log.verbose('Writing environment keys...');

	var outputFilename = options.packageEnvFilepath;

	options.fn.envHelper.writeKeys(outputFilename, options.environmentKeys);

	cb(null, options);
}


function zipDirectory(options, cb){
	var outputFilename = options.packageFilepath;

	if(fs.existsSync(outputFilename)) fs.unlinkSync(outputFilename);

	options.fn.zipHelper.zipDir(options.tmpDir, outputFilename, function(e, r){ cb(e, options) });
}

//--
//return options.fn.readEnvZipAndUpload({version: version}, cb);
